import sys
import os
import re
import requests
from bs4 import BeautifulSoup
from urllib.parse import urlparse

FILENAME = 'Filename'
ARTIST = 'Artist'
TITLE = 'Title'
DESCRIPTION = 'Description'
LICENSE_TYPE = 'License Type'
PAGE_URL = 'Page URL'
IMAGE_URL = 'Image URL'
ARTIST_URL = 'Artist URL'
KEYS = [FILENAME, ARTIST, TITLE, DESCRIPTION, LICENSE_TYPE, PAGE_URL, IMAGE_URL, ARTIST_URL]
CSV_HEADER = ', '.join(KEYS)

HEADERS = {'User-Agent': 'Mozilla/5.0'}

SAVE_IMAGE_DIR = './images'

def print_headers(spamwriter):
    '''Print the headers in a csv format.'''
    spamwriter.writerow(KEYS)


def get_content(url):
    '''
    Parse the given url and print the data as one line in a csv format to stdout.
    '''
    try:
        # TODO read robots.txt
        response = requests.get(url, headers=HEADERS)
    except requests.exceptions.RequestException as e:
        exit_error("Something wrong with {}".format(url), e)

    if response.status_code != 200:
        exit_error("Status code {} received from {}".format(response.status_code, url))

    try:
        content = parse_html_text(response.text)
    except ValueError as e:
        exit_error('Cannot parse {}'.format(url))

    return content


def parse_html_text(html_text):
    '''Parse the given html content.'''
    soup = BeautifulSoup(html_text, 'html.parser')

    content = {}
    content[FILENAME] = get_filename(soup)
    content[ARTIST] = get_artist_name(soup)
    content[TITLE] = get_image_title(soup)
    content[DESCRIPTION] = get_description(soup)
    content[LICENSE_TYPE] = get_license_type(soup)
    content[PAGE_URL] = get_page_url(soup)
    content[IMAGE_URL] = get_image_url(soup)
    content[ARTIST_URL] = get_artist_url(soup)

    return content


def get_filename(soup):
    # example: <meta property="og:image" content="https://images.freeimages.com/images/previews/randomstring/filename" />
    return get_image_url(soup).split('/')[-1]


def get_artist_name(soup):
    # example: <a href="/photographer/username-randomnumbers" id="photographer-name">Artist's Name</a>
    artist_tag = soup.find(id='photographer-name')
    if artist_tag is None:
        raise ValueError("Cannot find artist's name.")
    return artist_tag.string

def get_image_title(soup):
    # example: <meta property="og:url" content="https://www.freeimages.com/photo/title-randomnumbers"/>
    og_url = soup.find('meta', {'property': 'og:url'})
    if og_url is None:
        raise ValueError("Cannot find page's url.")
    url = og_url['content']
    title_with_numbers = url.split('/')[-1].split('-')
    title = ' '.join(title_with_numbers[:-1]).title()
    return title

def get_description(soup):
    # example: <meta property="og:description" content="A description"/>
    og_description = soup.find('meta', {'property': 'og:description'})
    if og_description is None:
        raise ValueError("Cannot find image's description.")
    description = og_description['content']
    return description

def get_license_type(soup):
    '''
    example:
    <script type="text/javascript">
    var tracking_data = {
        'page_name': "FreeImages|Detail|Free|English",
        'license_type': "free",
        'site_nickname': "freeimages",
        'host': "www.freeimages.com",
        'language': "en-us",
    };
    </script>
    '''
    p = re.compile('\'license_type\': "(.*?)"')
    for script in soup.find_all('script', {'type': 'text/javascript'}):
        m = p.search(script.text)
        if m is not None:
            return m.group(1)
    raise ValueError("Cannot find license type.")

def get_page_url(soup):
    # example: <meta property="og:url" content="https://www.freeimages.com/photo/title-randomnumbers"/>
    og_url = soup.find('meta', {'property': 'og:url'})
    if og_url is None:
        raise ValueError("Cannot find page's url.")
    url = og_url['content']
    return url

def get_image_url(soup):
    # example: <meta property="og:image" content="https://images.freeimages.com/images/previews/randomstring/filename" />
    og_image = soup.find('meta', {'property': 'og:image'})
    if og_image is None:
        raise ValueError("Cannot find image's url.")
    url = og_image['content']
    return url

def get_artist_url(soup):
    # example: <a href="/photographer/username-randomnumbers" id="photographer-name">Artist's Name</a>
    artist_tag = soup.find(id='photographer-name')
    if artist_tag is None:
        raise ValueError("Cannot find artist's url.")
    artist_url_path = artist_tag['href']

    # get this page's url to obtain hostname
    page_url = get_page_url(soup)

    # construct artist's url
    parsed_url = urlparse(page_url)
    url_scheme_host = parsed_url.scheme + '://' + parsed_url.hostname
    artist_url = url_scheme_host + artist_url_path

    return artist_url

def print_contents(spamwriter, contents):
    ''' 'contents' is a list of dictionaries, each has keys 'KEYS'. '''
    for c in contents:
        values = []
        for key in KEYS:
            values.append(c[key])
        spamwriter.writerow(values)

def change_filenames(contents, images_name):
    '''
    Change the filename of each entry in 'contents' to
    images_name001, images_name002, and so on.
    '''
    num = 1
    for c in contents:
        original_filename, extension = os.path.splitext(c[FILENAME])
        filename = "{}{:03}{}".format(images_name, num, extension)
        c[FILENAME] = filename
        num += 1

def save_images(contents):
    '''
    Download images to SAVE_IMAGE_DIR directory.
    'contents' is a list of dictionaries, each has keys 'KEYS'.
    '''

    # copied from https://stackoverflow.com/questions/273192/how-can-i-create-a-directory-if-it-does-not-exist
    if not os.path.exists(SAVE_IMAGE_DIR):
        os.makedirs(SAVE_IMAGE_DIR)

    for c in contents:
        save_image(c[IMAGE_URL], c[FILENAME])

def save_image(image_url, filename):
    '''Save the content of the given url to a file inside SAVE_IMAGES_DIR.'''
    try:
        # TODO read robots.txt
        response = requests.get(image_url)
    except requests.exceptions.RequestException as e:
        exit_error("Something wrong with {}".format(url), e)
    if response.status_code != 200:
        exit_error("Status code {} received from {}".format(response.status_code, url))

    with open(os.path.join(SAVE_IMAGE_DIR, filename), 'wb') as f:
        f.write(response.content)

def exit_error(message, exception=None):
    '''Print the message and exception, then exit.'''
    if exception is not None:
        message = '{}\nException: {}'.format(message, str(exception))
    sys.exit(message)
