# freeimages-parser

A parser written in Python 3 that parse data from a given list of line separated [freeimages.com](https://www.freeimages.com/) URLs and print the result in a csv format with eight fields to stdout.

These fields are:

1. The image's filename
2. The artist's name
3. The image's title
4. The image's description
5. The image's license type
6. The page's URL
7. The image's URL
8. The artist's URL

It is also able to download the images to folder `./images/` using the optional argument `-s`.

## Requirements

This parser uses the [requests](http://docs.python-requests.org/) and [Beautiful Soup 4](https://www.crummy.com/software/BeautifulSoup/bs4/doc/) libraries.

There are two simple ways to install these python libraries: using pip or [pipenv](https://docs.pipenv.org/):

- To install using pip:

```
$ pip install requests
$ pip install beautifulsoup4
```

- To install using pipenv and run the virtual environment:

```
$ pipenv install
$ pipenv shell
```


## Run Instructions

Let `input.txt` be a text file with the following content:
```
https://www.freeimages.com/photo/golf-ball-on-the-grass-1520778
https://www.freeimages.com/photo/mountain-lake-1555645
```

To parse the URLs on `input.txt`:

```$ python freeimages_parser.py < input.txt```

or

```$ python freeimages_parser.py -i input.txt```

### Options

```
usage: freeimages_parser.py [-h] [-i INPUT_FILE] [-s] [-n IMAGES_NAME]

Parse data from a given list of line separated freeimages.com URLs from stdout
and print the result in csv format to stdout.

optional arguments:
  -h, --help            show this help message and exit
  -i INPUT_FILE, --input_file INPUT_FILE
                        Read input from INPUT_FILE instead of stdin.
  -s                    Save images to folder ./images/
  -n IMAGES_NAME, --images_name IMAGES_NAME
                        The images will be downloaded as images_name001,
                        images_name002, and so on. By default, the images will
                        be saved using their original name. Only works with
                        the '-s' argument.
```


### Unit Tests

To run unit tests on the parser, first go to the `tests` directory and run the test script:

```
$ cd tests/
$ python test_parse.py
```