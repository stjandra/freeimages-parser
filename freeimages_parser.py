#!/usr/bin/env python

'''
Parse data from a given list of line separated freeimages.com URLs supplied from stdout and print the result in csv format to stdout.

These data are: image filename, artist's name, image title, description, license type, the page's URL, image's URL, and artist's URL
'''

import sys
import argparse
import csv
from freeimages_parser.parse import print_headers, get_content, print_contents, change_filenames, save_images

def get_args():
    '''Get command line arguments.'''
    parser = argparse.ArgumentParser(description="Parse data from a given list of line separated freeimages.com URLs from stdout and print the result in csv format to stdout.")
    parser.add_argument('-i', '--input_file', help="Read input from INPUT_FILE instead of stdin.", required=False)
    parser.add_argument('-s', help="Save images to folder ./images/", action='store_true', default=False)
    parser.add_argument('-n', '--images_name', help="The images will be downloaded as images_name001, images_name002, and so on. By default, the images will be saved using their original name. Only works with the '-s' argument.", required=False)

    args = parser.parse_args()
    return args

def read_input(filename):
    '''
    Get list of URLs from the specified file.
    If no file is specified, read from stdin.
    '''
    if filename is not None:
        with open(filename) as f:
            content = f.readlines()
    else:
        content = sys.stdin.readlines()
    urls = [line.strip() for line in content]  # remove trailing '\n'
    urls = [url for url in urls if url]  # remove empty lines
    return urls

def main():
    args = get_args()
    urls = read_input(args.input_file)

    # use '\n' instead of DOS '\r\n'
    spamwriter = csv.writer(sys.stdout, lineterminator='\n')

    # parse contents
    contents = []
    for url in urls:
        contents.append(get_content(url))

    if args.s:
        if args.images_name is not None:
            change_filenames(contents, args.images_name)
        # download images
        save_images(contents)

    # print parsed data in csv format
    print_headers(spamwriter)
    print_contents(spamwriter, contents)

if __name__ == "__main__":
    main()
