import unittest
import sys
from bs4 import BeautifulSoup

sys.path.append("..")
from freeimages_parser import parse


HTML_FILE = './data/Free Golf Ball on the grass Stock Photo - FreeImages.com.html'
CONTENT = {}
CONTENT[parse.FILENAME] = 'golf-ball-on-the-grass-1520778.jpg'
CONTENT[parse.ARTIST] = 'Andy Steele'
CONTENT[parse.TITLE] = 'Golf Ball On The Grass'
CONTENT[parse.DESCRIPTION] = 'a golf ball resting on the grass with the flag in the photos'
CONTENT[parse.LICENSE_TYPE] = 'free'
CONTENT[parse.PAGE_URL] = 'http://www.freeimages.com/photo/golf-ball-on-the-grass-1520778'
CONTENT[parse.IMAGE_URL] = 'https://images.freeimages.com/images/previews/296/golf-ball-on-the-grass-1520778.jpg'
CONTENT[parse.ARTIST_URL] = 'http://www.freeimages.com/photographer/andysteele-32187'

class ParseTest(unittest.TestCase):
    def setUp(self):
        with open(HTML_FILE, 'r') as f:
            self.html_text = f.read()
        self.soup = BeautifulSoup(self.html_text, 'html.parser')

        self.soup_empty = BeautifulSoup('', 'html.parser')

    def test_get_filename(self):
        self.assertEqual(parse.get_filename(self.soup), CONTENT[parse.FILENAME])
        self.assertRaises(ValueError, parse.get_filename, self.soup_empty)

    def test_get_artist_name(self):
        self.assertEqual(parse.get_artist_name(self.soup), CONTENT[parse.ARTIST])
        self.assertRaises(ValueError, parse.get_artist_name, self.soup_empty)

    def test_get_image_title(self):
        self.assertEqual(parse.get_image_title(self.soup), CONTENT[parse.TITLE])
        self.assertRaises(ValueError, parse.get_image_title, self.soup_empty)

    def test_get_description(self):
        self.assertEqual(parse.get_description(self.soup), CONTENT[parse.DESCRIPTION])
        self.assertRaises(ValueError, parse.get_description, self.soup_empty)

    def test_get_license_type(self):
        self.assertEqual(parse.get_license_type(self.soup), CONTENT[parse.LICENSE_TYPE])
        self.assertRaises(ValueError, parse.get_license_type, self.soup_empty)

    def test_get_page_url(self):
        self.assertEqual(parse.get_page_url(self.soup), CONTENT[parse.PAGE_URL])
        self.assertRaises(ValueError, parse.get_page_url, self.soup_empty)

    def test_get_image_url(self):
        self.assertEqual(parse.get_image_url(self.soup), CONTENT[parse.IMAGE_URL])
        self.assertRaises(ValueError, parse.get_image_url, self.soup_empty)

    def test_get_artist_url(self):
        self.assertEqual(parse.get_artist_url(self.soup), CONTENT[parse.ARTIST_URL])
        self.assertRaises(ValueError, parse.get_artist_url, self.soup_empty)

    def test_parse_html_text(self):
        self.assertEqual(parse.parse_html_text(self.html_text), CONTENT)
        self.assertRaises(ValueError, parse.parse_html_text, '')

    def test_change_filenames(self):
        contents = [{parse.FILENAME: 'image.jpg'}, {parse.FILENAME: 'picture.png'}]
        parse.change_filenames(contents, 'file')
        self.assertEqual(contents,
                [{parse.FILENAME: 'file001.jpg'}, {parse.FILENAME: 'file002.png'}])


if __name__ == '__main__':
    unittest.main()
